import React, { useEffect, useState } from 'react';
import { api } from './api';
import { Column, Header } from './components';

function App() {
  //Объявим state для хранения юзера
  // useState();

  //Объявим state для хранения колонок
  // useState();

  const [isAddColumnLoading, setIsAddColumnLoading] = useState(false);

  /**
   * Функция, которая делает запрос к API и создает новую колонку
   * (!) Обратите внимание, что мы храним
   *  в isAddColumnLoading статус выполнения запроса: загружается он или нет
   */
  const handleCreateColumn = async () => {
    setIsAddColumnLoading(true);

    /**
     * Здесь вызываем метод API на создание колонки
     * (!) После успешного создания колонки через API
     *  мы должны добавить ее в локальное состояние хранящее колонки,
     *  чтобы отобразить ее
     */

    /**
     * (?) Возможно, здесь еще стоит сбрасывать состояние инпута для имени
     *  новой колонки.
     */

    setIsAddColumnLoading(false);
  };

  /**
   * Метод для удаления карточки.
   * Он должен делать запрос к API
   *  и затем удалять карточку из локального состояния
   * @param {number} columnId
   */
  const handleDeleteColumn = async (columnId) => {
    /**
     * 1. API call
     * 2. Change columns state
     */
  };

  /**
   * Метод для создания карточки в колонке с ID columnId.
   * Делает запрос к API и добавляет карточку в локальное состояние
   * @param {number} columnId
   * @param {number} cardName
   */
  const handleCreateCard = async (columnId, cardName) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
  };

  /**
   * Метод для удаления карточки с ID cardId
   * Делает запрос к API и удаляет карточку и локального состояния
   * @param {number} cardId
   */
  const handleDeleteCard = async (cardId) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
  };

  /**
   * Функция подгрузки колонок и юзера -- начального состояния приложения.
   * Она должна запрашивать юзера и колонки с API, класть их в локальное состояние
   * */
  const fetchState = async () => {
    /**
     * 1. API call
     * 2. Change columns state
     * 3. Change user state
     */
  };

  /**
   * Task: На useEffect-е мы должны вызывать функцию fetchState.
   * Эта функция должна запрашивать с API: текущего юзера и все колонки
   */

  // useEffect()

  const renderColumns = () => {
    //Если колонки грузятся, то покажем загрузку
    if (!columns) {
      return <div>Loading...</div>;
    }

    //Если их нету -- пустой экран
    if (columns.length === 0) {
      return (
        <div>
          Nothing to show :( <br /> Create first column!
        </div>
      );
    }

    //Иначе -- отобразим все колонки
    return columns.map((column) => {
      return (
        <Column
          //Пропишем все пропсы, которые хочет Column (см. компонент Column)
          key={column.id}
        />
      );
    });
  };

  return (
    <div>
      {/* В Header мы должны передать имя текущего пользователя */}
      {/* {<Header name={}} */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: '24px',
        }}
      >
        {renderColumns()}
      </div>
      <div>
        <form>
          <input
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_input"
            placeholder="Column name"
            // Нужно хранить value-инпута в локальном состоянии
            value={}
            // И менять его на новое введенное значение на событии onChange
            // (!) новое введенное значение в подписчике можно получить так:
            // (event) => event.target.value (<-- tadaaa 🎉)
            onChange={}
          />
          <button
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_button"
            type="button"
            // По клику -- создаем колонку!
            onClick={}
            // Пока создание колонки "загружается" переводим колонку в disabled
            disabled={isAddColumnLoading}
          >
            Create
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
